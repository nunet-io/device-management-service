# Introduction

Orchestrator package will mainly manage remote jobs by sending messages to allocations and monitoring by recieving messages. It also deals with handling bids.

## Types and data models

### Allocation

As per initial [Detailed Job Orchestration Sequences I](https://nunet.gitlab.io/research/blog/posts/job-orchestration-details/), Orchestrator deals with the logic of deploying, monitoring, managing, remote jobs.